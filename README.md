# README #

Vivo CMS based project skeleton

### What is this repository for? ###

* Everything what you need for starting new Vivo based project.

### How do I get set up? ###

* run ./install.sh in root of your project
* open an URL in web browser
* follow installer instructions
* enjoy