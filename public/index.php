<?php
define('APP_DIR', dirname(__DIR__));

// INSTALLER INSTALLER INSTALLER INSTALLER INSTALLER INSTALLER INSTALLER INSTALLER INSTALLER INSTALLER
// comment or remove line below to disable installer
define('VIVO_INSTALL', !file_exists(APP_DIR . '/config/local.yml') || count(glob(APP_DIR . '/data/repository/*', GLOB_ONLYDIR)) < 2);
// uncomment line below to force installer
//define('VIVO_INSTALL', 1);
// INSTALLER INSTALLER INSTALLER INSTALLER INSTALLER INSTALLER INSTALLER INSTALLER INSTALLER INSTALLER

include APP_DIR . '/vendor/lundegaard/vivo/bootstrap.php';
